# https://github.com/microsoft/sarif-tutorials/blob/main/docs/2-Basics.md#results

# **************************************************************************************************
#  Perforce Software Inc
#  Outputs formatted data from API or CI analysis
#
#  $LastChangedBy: Dzuy Tran 
#  $LastChangedDate: 2023-08-03
#  $Version: 2.2
#
# Disclaimer: Please note that this software or software component is released by Perforce Software Inc
# on a non-proprietary basis for commercial or non-commercial use with no warranty. Perforce Software Inc
# will not be liable for any damage or loss caused by the use of this software. Redistribution is only
# allowed with prior consent.
#
# **************************************************************************************************

# **************************************************************************************************
# Change Log
# 1.2 - changed the schema for sarif format
# 1.3 - updated to convert states into correct sarif format, updated kwlp to show system issues
# 1.4 - updated kwciagent list command to better handle and output failures
# 1.5 - updated kwciagent list command to have option to show system or just local
# 1.6 - updated junit format to escape double quotes
# 1.7 - changed format of junit output to list per issue rather than per code
# 1.8 - added option when kwciagent is not in path
# 1.9 - use abs path on .kwlp and also show tools option.
# 2.0 - escape sensitive chars in junit
# 2.1 - escape sensitive chars in junit
# 2.2 - Update the import of urllib, and code changes to refelect the python3 syntax
# **************************************************************************************************

# **************************************************************************************************
# IMPORTS
# **************************************************************************************************

import ssl
import urllib.parse
import urllib.request
import argparse
import re
import json
import sys
import os
import socket
import datetime
import subprocess
import time
from xml.sax.saxutils import escape

g_version = 2.2
g_debug = False

# **************************************************************************************************
# GENERIC FUNCTIONS/CLASSES USED BY BOTH SCRIPTS
# **************************************************************************************************


def output(message, severity=''):
    if severity is None or severity == '':
        print('[' + str(datetime.datetime.now().strftime('%Y-%m-%d  %H:%M:%S')) + ']: ' + message)
    else:
        print('[' + str(datetime.datetime.now().strftime('%Y-%m-%d  %H:%M:%S')) + '] ' + severity + ': ' + message)


def print_progress(iteration, total, prefix='', suffix='', decimals=1, bar_length=100):
    format_str = "{0:." + str(decimals) + "f}"
    percents = format_str.format(100 * (iteration / float(total)))
    filled_length = int(round(bar_length * iteration / float(total)))
    bar = '*' * filled_length + '-' * (bar_length - filled_length)
    sys.stdout.write('\r%s |%s| %s%s %s' % (prefix, bar, percents, '%', suffix)),
    if iteration == total:
        sys.stdout.write('\n')
    sys.stdout.flush()


def start_message():
    global g_version
    print('--------------------------------------------------------------------------------')
    print(os.path.basename(__file__) + '\tversion: ' + str(g_version) + '\tcreated by: Perforce Software Inc')
    print('--------------------------------------------------------------------------------')


def end_message(code):
    if code == 0:
        print('--------------------------------------------------------------------------------')
        print('FINISHED: SUCCESSFUL')
        print('--------------------------------------------------------------------------------')
    else:
        print('--------------------------------------------------------------------------------')
        print('FINISHED: FAILED')
        print('--------------------------------------------------------------------------------')
    sys.exit(code)


class KwConnection:
    def __init__(self, url, ltoken=None, user=None):
        self.project_id = ''
        self.url = re.sub(r'^(https?://[\w.-]+:\d*)/.*$', r'\1', url)
        self.project = None
        m = re.match(r'^https?://[\w.-]+:\d*/([\w.-]+)$', url)
        if m:
            self.project = m.group(1)
        self.server = re.sub(r'^https?://([\w.-]+):\d*$', r'\1', self.url)
        if self.server == "localhost":
            self.server = socket.gethostname()
            self.url = self.url.replace("localhost", self.server)
        self.port = re.sub(r'^https?://[\w.-]+:(\d*)$', r'\1', self.url)
        self.api = self.url + "/review/api"
        self.user = user
        if ltoken:
            self.ltoken = ltoken
        else:
            self.ltoken = self._get_ltoken()
        if not self.ltoken:
            output("No Klocwork token found please run \"kwauth --url %s\"" % self.url, "ERROR")
            sys.exit(1)

    def search(self, action, query=None, opts=None, no_project=False):
        vals = {"action": action,
                "user": self.user}
        if self.project and not no_project:
            vals["project"] = self.project
        if query:
            vals["query"] = query
        if opts:
            opts.update(vals)  # Odd processing to ensure opts don't overwrite existing vals
            vals = opts
        return self._query(vals)

    def _query(self, vals):
        vals["ltoken"] = self.ltoken
        data = urllib.parse.urlencode(vals)
        request = urllib.request.Request(self.api, str.encode(data))
        try:
            response = urllib.request.urlopen(request)
            return self.decode(response.read().decode("utf-8"))
        except urllib.request.HTTPError as e:
            print("ERROR: HTTP Connection Error code: %s" % str(e))
            sys.exit(1)
        except urllib.request.URLError as e:
            print("ERROR: URL Error: %s" % str(e))
            sys.exit(1)

    def _get_ltoken(self):
        aliases = []
        for alias in socket.gethostbyname_ex(self.server):
            if isinstance(alias, list):
                for item in alias:
                    aliases.append(item.lower())
            else:
                aliases.append(alias.lower())
        token_file = ''
        if os.environ.get('KLOCWORK_LTOKEN') is not None:
            token_file = os.environ.get('KLOCWORK_LTOKEN')
        else:
            user_home = os.path.expanduser("~")
            if user_home is not None and user_home != '':
                token_file = os.path.join(user_home, ".klocwork", "ltoken")
            else:
                output("Unable to get users home directory", "WARNING")
				
        if not os.path.exists(token_file):
            output("Klocwork ltoken does not exist please run 'kwauth':" + token_file, "WARNING")
        ltoken_file = self._open_file(token_file, 'r')

        for line in ltoken_file:
            print(line)
            line_split = line.rstrip().split(';')
            if line_split[0].lower() in aliases \
                    and line_split[1].lower() == self.port.lower():
                self.user = line_split[2]
                ltoken_file.close()
                return line_split[3]
        ltoken_file.close()
        return ''

    @staticmethod
    def decode(text):
        text = text.replace("\u003d", "=")
        text = text.replace("\u0027", "'")
        return text

    @staticmethod
    def _open_file(name, x):
        f = None
        try:
            f = open(name, x)
        except IOError as e:
            output(e, "ERROR")
        return f


def check_project_exists(kwdb):
    global g_debug
    projects = []
    if g_debug:
        output('Checking that the project ' + kwdb.project + ' exists on the server', 'DEBUG')
        output('Getting list of projects', 'DEBUG')
    project_list = kwdb.search("projects", no_project=True)
    if project_list is not None:
        for line in project_list.split('\n'):
            if line:
                projects.append(json.loads(line))
        for project in projects:
            if 'status' in project.keys():
                if project['status'] == 403:
                    output(project['message'], 'ERROR')
                    return False
            if project['name'] == kwdb.project:
                kwdb.project_id = project['id']
                return True
    output('Failed to find the project ' + kwdb.project + ' on the Klocwork server', 'ERROR')
    return False


def check_builds_exists(kwdb):
    global g_debug
    builds = []
    if g_debug:
        output('Checking that the project ' + kwdb.project + ' contains build data', 'DEBUG')
        output('Getting list of build', 'DEBUG')
    for line in kwdb.search("builds").split('\n'):
        if line:
            builds.append(json.loads(line))
    if len(builds) > 0:
        for build in builds:
            return { 'name': build['name'], 'date': build['date']/1000 }
    output(kwdb.project + ' contains no build data', 'ERROR')
    return None


def check_view_exists(kwdb, view):
    global g_debug
    views = []
    if g_debug:
        output('Checking that the view ' + view + ' exists', 'DEBUG')
        output('Getting list of views', 'DEBUG')
    for line in kwdb.search("views").split('\n'):
        if line:
            views.append(json.loads(line))
    if len(views) > 0:
        for v in views:
            if view == v['name']:
                return v['query']
    output(kwdb.project + ' does not contain the view ' + view, 'ERROR')
    return None


def parse_args():
    parser = argparse.ArgumentParser(prog=os.path.basename(__file__),
                                     description="Outputs formatted data from API or CI analysis")
    parser.add_argument('-a', '--analysis-data', required=True, default='api', const='api', nargs='?',
                        choices=['api', 'kwlp'],
                        help='api for integration analysis and kwlp for kwciagent analysis (default: %(default)s)')
    parser.add_argument('-f', '--format', required=True, default='sarif', const='sarif', nargs='?',
                        choices=['sarif', 'junit'],
                        help='creates a json sarif report or junit xml report (default: %(default)s)')
    parser.add_argument("--url", nargs='?', required=False, metavar="URL",
                        help="http[s]://<server>:<port>/<project>")
    parser.add_argument("--kwlp", nargs='?', required=False, metavar="kwlp",
                        help=".kwlp folder location")
    parser.add_argument("-o", nargs='?', required=False, metavar="filename",
                        help="default is kw.sarif")
    parser.add_argument("--view", nargs='?', required=False, metavar="view-name",
                        help="specify the name of the view to filter on")
    parser.add_argument("--user", nargs='?', required=False, metavar="User Name",
                        help="specify the name of the authenticated Klocwork user to use")
    parser.add_argument("--token", nargs='?', required=False, metavar="Token",
                        help="specify the token of the authenticated Klocwork user to use")
    parser.add_argument("--system", action="store_true", default=False, help='if using .kwlp data show system issues (defaults to local only)')
    parser.add_argument("--debug", action="store_true", default=False, help=argparse.SUPPRESS)
    parser.add_argument("--tools", nargs='?', required=False, metavar="path",
                        help='if kwciagent is not in path, specify the bin folder location')
    return parser.parse_args()


def get_issues_api(kwdb, view):
    output('getting issue data from component')
    opts = None
    if view is not None:
        opts = {'view': view}
    response = kwdb.search("search", query='grouping:off', opts=opts)
    api_data = []
    if response is not None:
        for line in response.split('\n'):
            if line:
                api_data.append(json.loads(line))
    return api_data


def get_version_api(kwdb, buildname):
    api_data = []
    version = None
    response = kwdb.search("project_configuration", opts={'build': buildname})
    if response is not None:
        for line in response.split('\n'):
            if line:
                api_data.append(json.loads(line))
        for item in api_data:
            if 'version' in item:
                version = item['version']
    return version


def get_version_kwlp(tools):
    version = ''
    kwciagent = ''
    if tools != '':
        kwciagent = os.path.join(tools, 'kwciagent')
    else:
        kwciagent = 'kwciagent'
    cmd = [kwciagent, '--version']
    p = subprocess.Popen(cmd, shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    for line in p.stdout:
        if line.strip().startswith('Klocwork'):
            version = line.strip()
            break
    return version


def format_kw_data_junit(data_source, timestamp, issues, xmlfilename, buildname):
    testcases = {}
    if data_source == 'api':
        status_key = 'status'
        analysistype = 'integration'
    elif data_source == 'kwlp':
        status_key = 'citingStatus'
        analysistype = 'ci'
    else:
        status_key = ''
        analysistype = ''
    total_open_issues = 0
    total_ignored_issues = 0
    for issue in issues:
        if issue[status_key].lower() == 'analyze' or issue[status_key].lower() == 'fix':
            total_open_issues += 1
            filepath = str(issue['file']).replace('\\', '/').split('/')
            filename = filepath[len(filepath) - 1]
            text = issue['severity'] + ': ' + issue['message'] + '\n' + 'Category: Klocwork Analysis - ' + issue[
                'code'] + '\n' + 'File: ' + issue['file']
            if 'line' in issue.keys():
                message = filename + ':' + str(issue['line']) + ' ' + issue['message']
                text += '\n' + 'Line: ' + str(issue['line'])
            else:
                message = filename + ' ' + issue['message']
            if '"' in message:
                message = message.replace('"', '&quot;')
            if '"' in text:
                text = text.replace('"', '&quot;')
            failure = {'message': message, 'type': issue['severity'], 'text': text, 'filename': filename}
            if issue['id'] in testcases.keys():
                output('id '+issue['id']+' already added', 'WARNING')
            else:
                testcases[str(issue['id'])] = failure
        else:
            total_ignored_issues += 1
    if xmlfilename is None:
        dataFile = open('kwjunit.xml', 'w')
    else:
        dataFile = open(xmlfilename, 'w')
    dataFile.write('<?xml version="1.0" encoding="UTF-8" ?>\n')
    dataFile.write('<testsuites id="'+buildname+'_'+str(timestamp)+'" name="Klocwork Report ('+buildname+ ' ' +str(datetime.datetime.fromtimestamp(timestamp))+')" tests="'+str(total_open_issues+total_ignored_issues)+'" failures="'+str(total_open_issues)+'" time="0.001">\n')
    dataFile.write('\t<testsuite id="klocwork.'+analysistype+'" name="Klocwork '+analysistype+' analysis" tests="'+str(total_open_issues+total_ignored_issues)+'" failures="'+str(total_open_issues)+'" time="0.001">\n')
    for id in testcases.keys():
        dataFile.write('\t\t<testcase id="klocwork.'+analysistype+'" name="Klocwork Issue ID:'+str(id)+', '+escape(testcases[str(id)]['message'])+'" file="'+testcases[str(id)]['filename']+'" time="0.001">\n')
        dataFile.write('\t\t\t<failure message="'+escape(testcases[str(id)]['message'])+'" type="'+escape(testcases[str(id)]['type'])+'">\n'+escape(testcases[str(id)]['text'])+'\n\t\t\t</failure>\n')
        dataFile.write('\t\t</testcase>\n')
    dataFile.write('\t</testsuite>\n')
    dataFile.write('</testsuites>\n')
    dataFile.close()


def refBlock(codeFlow, traceBlocks, traceBlock, nestingLevel):
    for line in traceBlock['lines']:
        codeFlow.append(
            {
                'location': {
                    "message": {
                        "text": line['text']
                    },
                    "physicalLocation": {
                            "region": {
                                "startLine": line['line']
                            }, 
                            "artifactLocation": {
                                "uri": urllib.parse.urljoin('file:', urllib.pathname2url(traceBlock['file']))
                            }
                        }
                },
                "nestingLevel": nestingLevel,
                "executionOrder": len(codeFlow)+1
            }
        )
        if 'refId' in line.keys():
            nestingLevel += 1
            for nestedBlock in traceBlocks:
                if nestedBlock['id'] == line['refId']:
                    refBlock(codeFlow, traceBlocks, nestedBlock, nestingLevel)


def format_kw_data_sarif(data_source, version, issues, filename, url=None, buildname=None):
    if data_source == 'api':
        status_key = 'status'
    elif data_source == 'kwlp':
        status_key = 'citingStatus'
    else:
        status_key = ''
    sarif = { "version": "2.1.0",
              "$schema": "https://docs.oasis-open.org/sarif/sarif/v2.1.0/cos02/schemas/sarif-schema-2.1.0.json",
              "runs": [
                  {"tool": {"driver": {"name": "Klocwork", "fullName": "Klocwork Static Analysis", "version": version}}, "results": []}
              ]
            }
    if url is not None:
        sarif['runs'][0]['tool']['driver']['informationUri'] = url
    if buildname is not None:
        sarif['runs'][0]['automationDetails'] = {"id": buildname}
    for issue in issues:
        state = ''
        if issue['state'].lower() == 'existing':
            state = 'unchanged'
        elif issue['state'].lower() == 'new':
            state = 'new'
        else:
            state = 'absent'
        tmp = {
            "fingerprints": {
                "kwid": str(issue['id'])
            },
            "ruleId": issue['code'],
            "message": {"text": issue['message']},
            "baselineState": state,
            "locations": [{
                "physicalLocation":{
                    "artifactLocation": {
                        "uri": urlparse.urljoin('file:', urllib.pathname2url(issue['file']))
                    }
                }
            }]
        }
        if 'line' in issue.keys():
            tmp['locations'][0]['physicalLocation']['region'] = {'startLine': issue['line']}
        if issue['severityCode'] == 1 or issue['severityCode'] == 2:
            tmp["level"] = "error"
        else:
            tmp["level"] = "warning"
        if issue[status_key].lower() != 'analyze' or issue[status_key].lower() != 'fix':
            tmp["suppressions"] = [{"kind": "external"}]
        if 'trace' in issue.keys() and 'traceBlocks' in issue['trace'].keys() and len(issue['trace']['traceBlocks']) > 0:
            codeFlow = []
            # for traceBlock in issue['trace']['traceBlocks']:
            refBlock(codeFlow, issue['trace']['traceBlocks'], issue['trace']['traceBlocks'][0], 0)
            tmp["codeFlows"] = [{'threadFlows': [{'locations': codeFlow}]}]
        sarif["runs"][0]["results"].append(tmp)
    if filename is None:
        dataFile = open('kw.sarif', 'w')
    else:
        dataFile = open(filename, 'w')
    dataFile.write(json.dumps(sarif, indent=4))
    dataFile.close()


def get_issues_kwlp(kwlp, system, tools):
    global g_debug
    kwciagent = ''
    if tools != '':
        kwciagent = os.path.join(tools, 'kwciagent')
    else:
        kwciagent = 'kwciagent'
    data = ''
    if system:
        flag = '-y'
    else:
        flag = '-Y'
    cmd = [kwciagent, 'list', '-pd', kwlp, flag, '-F', 'json']
    if g_debug:
        output('using command: '+str(cmd), 'DEBUG')
    p = subprocess.Popen(cmd, shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    for line in p.stdout:
        data += line.strip()
    try:
        issues = json.loads(data)
    except:
        output('running kwciagent list failed', 'ERROR')
        output(data)
        return {}
    return issues

def main():
    global g_debug
    start_message()
    args = parse_args()
    g_debug = args.debug
    system = args.system
    tools = args.tools
    if tools is None:
        tools = ''
    data_source = args.analysis_data
    if data_source == 'api':
        if args.url is None or args.url == '':
            output("Url argument is required with the api data source option", "ERROR")
            end_message(1)
        if not re.match(r'^https?://[\w.-]+:\d*/[\w.-]+$', args.url):
            output("Invalid url: %s" % args.url, "ERROR")
            output("http[s]://<server>:<port>/<project>", "ERROR")
            end_message(1)
    elif data_source == 'kwlp':
        if args.kwlp is None or args.kwlp == '':
            output("kwlp argument is required with the kwlp data source option", "ERROR")
            end_message(1)
        if not os.path.isdir(args.kwlp):
            output("Invalid kwlp folder: %s" % args.url, "ERROR")
            end_message(1)
        output('setting kwlp to: '+os.path.abspath(args.kwlp))
        args.kwlp = os.path.abspath(args.kwlp)

    if data_source == 'api':
        output("connecting to Klocwork API customer project")
        kwdb = KwConnection(args.url, args.token, args.user)
        if kwdb is None:
            output('Unable to create a connection to the Klocwork server', 'ERROR')
            end_message(1)
        if check_project_exists(kwdb):
            build_data = check_builds_exists(kwdb)
            if build_data is not None:
                output(kwdb.project + ' is valid and contains build data')
            else:
                end_message(1)
        else:
            end_message(1)
        output("note: all issue id's referenced are with grouping:off")
        if args.format == 'sarif':
            format_kw_data_sarif(data_source, get_version_api(kwdb, build_data['name']), get_issues_api(kwdb, args.view), args.o, kwdb.url, build_data['name'])
        elif args.format == 'junit':
            format_kw_data_junit(data_source, build_data['date'], get_issues_api(kwdb, args.view), args.o, build_data['name'])
    elif data_source == 'kwlp':
        if args.format == 'sarif':
            format_kw_data_sarif(data_source, get_version_kwlp(tools), get_issues_kwlp(args.kwlp, system, tools), args.o)
        elif args.format == 'junit':
            format_kw_data_junit(data_source, int(time.time()), get_issues_kwlp(args.kwlp, system, tools), args.o, 'ci')

    end_message(0)


# **************************************************************************************************
# RUN SCRIPT
# **************************************************************************************************
if __name__ == '__main__':
    main()
# **************************************************************************************************

